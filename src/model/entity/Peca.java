package model.entity;

import java.util.GregorianCalendar;

public class Peca {
	private Long id;
	private String descricao;
	private Integer categoria;
	private Integer marca;
	private Double valor; 
	private Long qtd;
			
	/* Construtor Vazio */
	
	public Peca() {
		super();
	}

	public Peca(Long id, String descricao, Integer categoria, Integer marca, Double valor, Long qtd) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.categoria = categoria;
		this.marca = marca;
		this.valor = valor;
		this.qtd = qtd;		
	}

	@Override
	public String toString() {
		return "Peça [id=" + id + ", descricao=" + descricao + ", categoria=" + categoria
				+ ", marca=" + marca + " valor=" + valor + ", qtd=" + qtd + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getMarca() {
		return marca;
	}

	public void setMarca(Integer marca) {
		this.marca = marca;
	}	

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Long getQtd() {
		return qtd;
	}

	public void setQtd(Long qtd) {
		this.qtd = qtd;
	}
}
