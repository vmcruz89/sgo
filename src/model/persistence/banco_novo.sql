/* Roteiro
 * Falta consulta alterar e excluir cliente
 * Listar tabela com dados produto (resumo) e abrir em detalhes todos os dados
 * Falta altera��o nos dados do usu�rio/cliente
 * Falta exclus�o (como vou fazer isso?)
 * colocar senha criptografada com md5 no banco
 * 
 * Fazer o Manter produto
 * 
 * Montar a tela de pedido
 * TEMA para carrinho de compras no bootstrap
 * http://blog.neweb.co/Top-10-temas-de-inicializa%C3%A7%C3%A3o-ecommerce/?lang=pt
 * 
 * 
 * https://www.caelum.com.br/apostila-vraptor-hibernate/criando-o-carrinho-de-compras/#13-1-o-modelo-do-carrinho
 * MATERIAL muito bom : http://docente.ifrn.edu.br/fellipealeixo/disciplinas/tads-2012/desenvolvimento-de-sistemas-web
 * 
 * 
 * C�digo de banco 1062 - PRIMARY KEY j� existe
 * 
 * 
 * SISTEMA GERENCIADOR DE CHAMADOS*/

drop database if exists SGC;

create database SGC;

use SGC;

CREATE TABLE TB_Usuario (
	Id INT(4) auto_increment NOT NULL PRIMARY KEY,
	IdFuncional VARCHAR(25),
	Nome VARCHAR(255),
	Email VARCHAR(255),
	Senha VARCHAR(255),
	UltimaAtualizacao TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	Nivel INT(2) DEFAULT '1',
	Cargo VARCHAR(60) DEFAULT NULL,
	Setor INT(4) DEFAULT NULL,  
	Ramal INT(4) DEFAULT NULL,
	Celular VARCHAR(15) DEFAULT NULL,
	DtNascimento TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	StatusUsuarios tinyINT(1) NOT NULL
);
DESC TB_Usuario;

CREATE TABLE TB_NivelAcesso(
	Id INT PRIMARY KEY,
	Descricao VARCHAR(60)	
);
DESC TB_NivelAcesso;

CREATE TABLE TB_Setor (
	Id INT(4) NOT NULL PRIMARY KEY,
	Descricao VARCHAR(60) NOT NULL,
	Andar INT(3) NOT NULL,
	Ramal INT(4) NOT NULL
);
DESC TB_Setor;

CREATE TABLE TB_Prioridade(
	Id INT PRIMARY KEY,
	Descricao VARCHAR(60)	
);
DESC TB_Prioridade;

CREATE TABLE TB_Situacao(
	Id INT PRIMARY KEY,
	Descricao VARCHAR(60)
);
DESC TB_Situacao;

CREATE TABLE TB_Status(
	Id INT(2) PRIMARY KEY,
	Descricao VARCHAR(60)	
);
DESC TB_Status;

CREATE TABLE TB_Chamado(
	Id BIGINT PRIMARY KEY,	
	IdUsuario INT(4) NOT NULL,	
	DtAbertura TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	DtFechamento TIMESTAMP NULL,
	IdStatus INT(2) DEFAULT '1',
	Descricao VARCHAR(200) NOT NULL,  
	
	FOREIGN KEY fk_Usuario(IdUsuario) REFERENCES TB_Usuario(Id),
	FOREIGN KEY fk_Status(IdStatus) REFERENCES TB_Status(Id)
);
DESC TB_Chamado;


CREATE TABLE TB_Chamado_Log(
	Id BIGINT PRIMARY KEY,
	IdChamado BIGINT,
	IdPrioridade INT DEFAULT '1',
	IdSituacao INT DEFAULT '1',
	IdAnalista INT NOT NULL,
	IdProblema INT NOT NULL,	
	DtAtualizacao TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,	
	
	FOREIGN KEY fk_chamado(IdChamado) REFERENCES TB_Chamado(Id),
	FOREIGN KEY fk_prioridade(IdPrioridade) REFERENCES TB_Prioridade(Id),
	FOREIGN KEY fk_situacao(IdSituacao) REFERENCES TB_Situacao(Id),
	FOREIGN KEY fk_analista(IdAnalista) REFERENCES TB_Usuario(Id)	
);
DESC TB_Chamado_Log;


CREATE TABLE TB_Resolucao(
	Id BIGINT PRIMARY KEY,
	IdChamadoLog BIGINT,
	Descricao VARCHAR(60),
	 
	FOREIGN KEY fk_chamadolog(IdChamadoLog) REFERENCES TB_Chamado_Log(Id)
);
DESC TB_Resolucao;