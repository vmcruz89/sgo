package model.persistence;

import java.sql.SQLException;

import model.entity.Peca;

public class PecaDao extends Dao{

/* Incluir o Produto */	
public boolean cadastrar(Peca p) throws Exception{
		boolean success = false;
		try{
			/* Abrir a conex�o com o banco de dados*/
			open(); 
			
			/* Retirar o auto commit */
			con.setAutoCommit(false);
				
			/* Inserir os dados do produto na tabela PEÇA */
			stmt = con.prepareStatement("insert into peca values(null,?,?,?,?,?)");
			stmt.setString(1, p.getDescricao());
			stmt.setInt(2, p.getCategoria());
			stmt.setInt(3, p.getMarca());
			stmt.setDouble(4, p.getValor());
			stmt.setLong(5, p.getQtd());
						
			stmt.execute();
			success = true;
			con.commit();
			
		}catch(SQLException e1){
			if (con != null){
				try { 
					System.out.println("Rollback efetuado na transa��o");
					con.rollback();
					success = false;
					System.out.println("C�digo de Erro: " +e1.getErrorCode()+
							"  Mensagem de Erro =  "+e1.getMessage());
				} catch(SQLException e2) {
					System.err.print("Erro na transa��o!"+e2); 
				} 
			}
		}finally{
			close();
		}
		return success;
}

/* Consultar o Produto pelo C�digo */
public Peca buscarPorId(Long id)throws Exception{
	Peca peca = null;
	try{
		open();
		
		stmt = con.prepareStatement("select * from peca where id = ?");
		stmt.setLong(1,id);
		
		rs = stmt.executeQuery();
		
		if(rs.next()){
			peca = new Peca(rs.getLong("id"), rs.getString("descricao"),
					rs.getInt("categoria"), rs.getInt("marca"), rs.getDouble("valor"),
					rs.getLong("qtd"));
		}
	}catch(SQLException e){
		e.printStackTrace();
	}finally{
		close();
	}		
	return peca;
}


/* Alterar o Produto */	
public boolean alterar(Peca p) throws Exception{
		boolean success = false;
		try{
			/* Abrir a conex�o com o banco de dados*/
			open(); 
		
			/* alterar os dados na tabela PRODUTO*/
			stmt = con.prepareStatement("update peca set descricao = ?, categoria = ?,"
					+ "marca = ?, valor  = ?, qtd = ? where id = ?");
			
			stmt.setString(1, p.getDescricao());
			stmt.setInt(2, p.getCategoria());
			stmt.setInt(3, p.getMarca());
			stmt.setDouble(4, p.getValor());
			stmt.setLong(5, p.getQtd());	
			stmt.setLong(0, p.getId()); //COLOCAR C�DIGO COMO UNEDIT NO FORM
						
			stmt.execute();
			success = true;
			
		}catch(SQLException e1){
			success = false;
			System.out.println("C�digo de Erro: " +e1.getErrorCode()+ "Mensagem de Erro =  "+e1.getMessage());
			
		}finally{
			close();
		}
		return success;
}

/* Excluir Produto */
public boolean excluir(Long id)throws Exception{
	boolean success = false;
	try{
		open();
		stmt = con.prepareStatement("delete from peca where id = ?");
		stmt.setLong(1, id);
		stmt.execute();
		success = true;
	}catch(SQLException e1){
		System.out.println("C�digo de Erro: " +e1.getErrorCode()+ "Mensagem de Erro =  "+e1.getMessage());
	}finally{
		close();
	}
	return success;
}

}
