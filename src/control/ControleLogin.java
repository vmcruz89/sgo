package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.persistence.UtilsBanco;

/**
 * Servlet implementation class ControleLogin
 */
//Servlet 3.0 aceita tanto com anota��o quanto pelo web.xml.
@WebServlet({"/ControleLogin", "/jsp/login.html"})
public class ControleLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public ControleLogin() {
        super();
    }
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		execute(request, response); /* inclu�do na aula em substitui��o a: 
		response.getWriter().append("Served at: ").append(request.getContextPath());*/
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		execute(request, response); /* inclu�do na aula em substitui��o a: doGet(request, response);*/
	}
	
	/* m�todo "execute" inserido na aula 
	 * Objetivo: ????????????
	 * */
	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{			
			String username=request.getParameter("login");
	    	String password=request.getParameter("senha"); 
	    	
	    	if((username.equals("user") && password.equals("123"))){ 
	    		response.sendRedirect("Home.jsp");    		
	    	}else{
				response.sendRedirect("CadastrarCliente.jsp");
			}
		}catch(Exception e){
			//tratar melhor ?????
			e.printStackTrace();
		}
	}
}