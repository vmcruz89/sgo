package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entity.Peca;
import model.persistence.PecaDao;
import model.persistence.UtilsBanco;

/**
 * Servlet implementation class ControleCliente
 */
//Servlet 3.0 aceita tanto com anota��o quanto pelo web.xml.
@WebServlet({"/ControlePeca", "/jsp/cadpeca.html", 
		"/jsp/consultarpeca.html", "/jsp/altpeca.html", "/jsp/excluirpeca.html"})
public class ControlePeca extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ControlePeca() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		execute(request, response); /* inclu�do na aula em substitui��o a: 
		response.getWriter().append("Served at: ").append(request.getContextPath());*/
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		execute(request, response); /* inclu�do na aula em substitui��o a: doGet(request, response);*/
	}
	
	/* m�todo "execute" inserido na aula */
	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			//Pegar a URL que foi executada
			String url = request.getServletPath();	        
			if(url.equalsIgnoreCase("/jsp/cadpeca.html")){
				cadastrar(request, response);
			}else if(url.equalsIgnoreCase("/jsp/consultarpeca.html")){
				consultar(request, response);
			}else if(url.equalsIgnoreCase("/jsp/altpeca.html")){
				alterar(request, response);
			}else if(url.equalsIgnoreCase("/jsp/excluirpeca.html")){
				excluir(request, response);				
			}else{
				response.sendRedirect("/");
			}
		}catch(Exception e){ 
			e.printStackTrace();
		}
	}

protected void cadastrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{
			/* Resgatar os dados do formulario: formPeca*/
			String descricao = request.getParameter("descrpeca");
			String categoria = request.getParameter("cbocategoria");
			String marca = request.getParameter("cbomarca");
			String valor = request.getParameter("valorpeca");								
			String qtd = request.getParameter("qtd");
			
			Peca peca = new Peca();
			
			/* Hospedar os dados do Peca na Bean Peca*/
			peca.setDescricao(descricao);
			peca.setCategoria(new Integer(categoria));
			peca.setMarca(new Integer(marca));			
			peca.setValor(new Double(UtilsBanco.prepararDouble(valor)));			
			peca.setQtd(new Long(qtd));
			
			/*OBS c�digo do peca ser� */
				
			if(new PecaDao().cadastrar(peca)){
				request.setAttribute("msg", 
				"<div class='alert alert-success'>Peca cadastrado com sucesso</div>");
			}else{
				request.setAttribute("msg", 
				"<div class='alert alert-danger'>Peca j� possui um cadastro no sistema</div>");
			}
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("msg", 
					"<div class='alert alert-danger'>Peca n�o cadastrada</div>");
		}finally{
			request.getRequestDispatcher("CadastrarPeca.jsp").forward(request, response);
		}
		
}

/* Consultar os dados de um Peca */
protected void consultar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	try{
		/* Pegar o c�digo digitado na tela */			
		Long id = new Long(request.getParameter("codconsulta"));
		
		/* Manter no campo codconsulta o valor buscado */
		request.setAttribute("codconsulta", id);
		
		/* Instanciar a classe DaoPeca */
		PecaDao pd = new PecaDao();
		
		/* Acionar o m�todo da classe Dao Peca que retorna os dados de um peca a 
		 * partir do seu c�digo*/
		Peca peca = pd.buscarPorId(id);
	
		if(peca == null){ //peca n�o encontrado
			request.setAttribute("msg", "<div class='alert alert-warning'>Peca "
					+ " nao existente</div>");
			request.getRequestDispatcher("ConsultarPecaBS.jsp").forward(request, response);
		}else{
			request.setAttribute("p", peca);
			request.setAttribute("op", "C"); //opera��o = consulta
			request.getRequestDispatcher("CadastrarPeca.jsp").forward(request, response);
		}
		
	}catch(Exception e){
		e.printStackTrace();
	}
}

protected void alterar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	try{
		
		/* Resgatar os dados do formulario: formPeca*/
		String id = request.getParameter("id");
		String descricao = request.getParameter("descrpeca");
		String categoria = request.getParameter("cbocategoria");
		String marca = request.getParameter("marca");		
		String valor = request.getParameter("valorpeca");							
		String qtd = request.getParameter("qtd");
		
		Peca peca = new Peca();
		
		/* Hospedar os dados do Peca na Bean Peca*/
		peca.setId(new Long(id));
		peca.setDescricao(descricao);
		peca.setCategoria(new Integer(categoria));
		peca.setMarca(new Integer (marca));		
		peca.setValor(new Double(UtilsBanco.prepararDouble(valor)));		
		peca.setQtd(new Long(qtd));
		
		if(new PecaDao().alterar(peca)){
			request.setAttribute("msg", 
			"<div class='alert alert-success'>Peca alterado com sucesso</div>");
			/*Setar os dados no request para parmanecer com as informa��es na tela ap�s o update*/
			request.setAttribute("p", peca);
			request.setAttribute("op", "C");
		}else{
			request.setAttribute("msg", 
			"<div class='alert alert-danger'>Erro ao alterar o peca.</div>");
		}
	}catch(Exception e){
		e.printStackTrace();
		request.setAttribute("msg", 
				"<div class='alert alert-danger'>Peca n�o cadastrada</div>");
	}finally{
		request.getRequestDispatcher("ConsultarPeca.jsp").forward(request, response);
	}
	
}

/* Listar Pecas */

/* Excluir Peca*/
protected void excluir(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	try{
		
		Long id = new Long(request.getParameter("id"));
		PecaDao pd = new PecaDao();
		Peca p = pd.buscarPorId(id);
		
		if(p == null){
			request.setAttribute("msg", "<div class='alert alert-warning'>Peca "
					+ " n�o encontrado</div>");
		}else{
			if(pd.excluir(id)){
				request.setAttribute("msg", "<div class='alert alert-success'>Peca "
						+ " exclu�do com sucesso!</div>");
			}else{
				request.setAttribute("msg", "<div class='alert alert-danger'>Peca "
						+ " n�o excluido</div>");
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		request.getRequestDispatcher("ConsultarPeca.jsp").forward(request, response);
	}
}
	
}
