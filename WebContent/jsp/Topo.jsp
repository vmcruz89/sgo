<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" 
	href="${pageContext.request.contextPath }/bootstrap-3.3.6-dist/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<div class="col-xs-12">
			<ul class="nav nav-pills nav-justified">
				<li><a href="index.jsp">HOME</a></li>
				<li><a href="cadastrar.jsp">CADASTRAR FUNCIONÁRIO</a></li>
				<li><a href="buscar.jsp">BUSCAR FUNCIONÁRIO</a></li>
			</ul>
		</div>
		
		<div class="col-xs-12">