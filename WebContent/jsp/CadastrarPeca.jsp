<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- local para trocar o encoding no eclipse - windows -> preference -> Web -> JSPFile -->    

<!-- ****** Incluído para a tela de consulta ******* -->
<%@ page import="view.UtilsTela" %>   
<!-- *********************************************** -->    
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Sistema de Gestão de Orçamentos - Cadastro de Peça</title>
	
	<!-- LINK PARA O ARQUIVO DE ESTILOS -->
	<!--  <link rel="stylesheet" href="../CSS/estilo.css" type="text/css" />-->
		
	<!-- LINK PARA A BIBLIOTECA JQUERY -->	
	<script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script>
	
	<!--LINK PARA O JQUERY MASKED INPUT -->
    <script src="../js/jquery-maskedinput/src/jquery.maskedinput.js" type="text/javascript"></script>
    
    <!--LINK PARA O JQUERY VALIDATION -->
    <script src="../js/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    
    <!--LINK PARA O JQUERY MASK MONEY -->
    <script src="../js/jquery-maskmoney/jquery.maskmoney.js" type="text/javascript"></script>
    
    <!--LINK PARA O JQUERY-UI - CALENDÁRIO - Fonte: https://jqueryui.com/ -->    
    <link rel="stylesheet" href="../js/jquery-ui-1.11.4.custom/jquery-ui.css">
    <script src="../js/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
  	<script src="../js/jquery-ui-1.11.4.custom/jquery-ui.js" type="text/javascript"></script>
    
    <!-- LINK PARA O BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="../bootstrap-3.3.6-dist/css/bootstrap.min.css">
	<script src="../bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
    
    <!-- http://webdevacademy.com.br/tutoriais/crud-com-bootstrap-3-parte2/    Boa ref -->
    
    <!-- LINK PARA O ARQUIVO JAVA SCRIPT DO PROJETO -->
	<script src="../js/lojaonlineJQ.js"></script>			
   
</head>
<body>

<div id="main" class="container"> <!-- CONTAINER BS -->

<hr>
	<div id="mensagens">
	${msg} <!-- INSERIDO NA AULA DE BACKEND -->	
	</div>
<hr>

<!-- ************************ JSTL -tela de consulta***************** -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> <!-- Não usado -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> <!-- Não usado -->
<!-- **************************************************************** -->

<!-- **** adaptar form de cadastro para a alteraçao ******************-->
<c:set var="url" value="cadpeca.html" />
<c:set var="btenviar" value="Enviar" />

<c:if test="${op eq 'C'}">
	<c:set var="url" value="altpeca.html" />
	<c:set var="btenviar" value="Alterar Peca" />
</c:if>
<!-- ***************************************************************** -->

<h1>Cadastro de Peca</h1>

<form name="formpeca" id="formpeca" role="form" action="${url}" method="post">

	<fieldset>	
		<legend class="control-label">Dados do Peca</legend>		
		
<%-- 		<div class="row">
			<div class="form-group col-md-4">
				<label for="inputdefault">Código:</label>
				<input type="text" name="codpeca" class="form-control" maxlength="8" size="8"
				value="${p.id}"
				/>
				<!-- O código será por geração automática -->									
			</div>
		</div> --%>
		
		<div class="row">
			<div class="form-group col-md-8">
				<label for="inputdefault">Descrição*:</label>				
				<input type="text" name="descrpeca" class="form-control" maxlength="100" size="100"
				value="${p.descricao}" />							
			</div>
		</div>					
		
		<div class="row">
			<div class="form-group col-md-3">
				<label for="inputdefault">Categoria do Peca*:</label> 
				<select id=cbocategoria class="form-control" name="cbocategoria">
					<option value="0"> </option>
					<option value="1" <c:if test= "${(p.categoria eq '1')}">selected</c:if>>Empilhadeira</option>			
					<option value="2" <c:if test= "${(p.categoria eq '2')}">selected</c:if>>Guindaste</option>					
				</select>
			</div>			
			<div class="form-group col-md-5">
				<label for="inputdefault">Marca*:</label>
				<select id=cbomarca class="form-control" name="cbomarca">
					<option value="1" <c:if test= "${(p.marca eq '1')}">selected</c:if>>Yale</option>			
					<option value="2" <c:if test= "${(p.marca eq '2')}">selected</c:if>>Still</option>
				</select>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group col-md-3">
				<label for="inputdefault">Valor*:</label>
				<input type="text" class="form-control dinheiro" name="valorpeca" maxlength="14" size="14"
				value="${p.valor}" />
			</div>
		</div>
		
		<div class="row">	
					
			<div class="form-group col-md-3">
				<label for="inputdefault">Quantidade em estoque*:</label>
				<input type="text" class="form-control sonums" name="qtd" maxlength="10"
				size="10" value="${p.qtd}" />
			</div>
		</div>	
		
		<div class="row">
			<div class="col-md-8">
				<input type="submit" value="${btenviar}" class="btn btn-primary" />
				
				<c:if test="${op eq 'C'}">	
					<a href="excluirpeca.html?id=${codconsulta}" 
						onclick="return confirm('Deseja excluir este Peca?')" 
								class="btn btn-danger">Excluir</a>
				</c:if>
				
				<input type="button" class="btlimpar" value="Limpar" class="btn btn-default" />
				
				<c:choose>
    				<c:when test="${op eq 'C' }">
                      	<input type="button" class="btn btn-default btvoltarcons" value="Voltar" />
         			</c:when>
         			<c:otherwise>
                    	<input type="button" class="btn btn-default btvoltar" value="Voltar" />
					</c:otherwise>
				</c:choose>
					
			</div>
		</div>
							
	</fieldset>
</form>
<hr>

</div>

</body>
</html>