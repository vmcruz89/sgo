<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="view.UtilsTela" %>   
<!-- *********************************************** -->
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
<!-- LINK PARA O ARQUIVO DE ESTILOS -->
	<!-- <link rel="stylesheet" href="CSS/estilo.css" type="text/css" /> -->
		
	<!-- LINK PARA A BIBLIOTECA JQUERY -->	
	<script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script>
	
	<!--LINK PARA O JQUERY MASKED INPUT -->
    <script src="../js/jquery-maskedinput/src/jquery.maskedinput.js" type="text/javascript"></script>
    
    <!--LINK PARA O JQUERY VALIDATION -->
    <script src="../js/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    
    <!--LINK PARA O JQUERY MASK MONEY -->
    <script src="../js/jquery-maskmoney/jquery.maskmoney.js" type="text/javascript"></script>
    
    <!--LINK PARA O JQUERY-UI - CALENDÁRIO - Fonte: https://jqueryui.com/ -->    
    <link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.css">
    <script src="../js/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
  	<script src="../js/jquery-ui-1.11.4.custom/jquery-ui.js" type="text/javascript"></script>
  	
  	<!-- LINK PARA O BOOTSTRAP -->
    <link rel="stylesheet" type="text/css" href="../bootstrap-3.3.6-dist/css/bootstrap.min.css">
	<script src="../bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
    
    <!-- http://webdevacademy.com.br/tutoriais/crud-com-bootstrap-3-parte2/ Boa ref -->
  	
    <!-- LINK PARA O ARQUIVO JAVA SCRIPT DO PROJETO -->
	<script src="../js/lojaonlineJQ.js"></script>	
<title>Insert title here</title>

<body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> <!-- Não usado -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> <!-- Não usado -->
<c:set var="url" value="login.html" />

	<div class="container">
		<div class="col-xs-12">
			<form action="${url}" method="post">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>SISTEMA DE GESTÃO DE ORÇAMENTOS</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							Login: 
							<input type="text" name="login" class="form-control" value="user">
						</div>
						<div class="form-group">
							Senha: 
							<input type="password" name="senha" class="form-control" value="123">
						</div>
					</div>
					<div class="panel-footer">
						<input type="submit" value="Entrar"  class="btn btn-info"> 
					</div>
				</div>
			</form>
			${msg }
		</div>
	</div>

</body>
</html>